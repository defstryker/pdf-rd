package main

import (
	"fmt"

	"github.com/signintech/pdft"
)

// ff - form fill for `form.pdf`

func main() {
	var pt pdft.PDFt
	err := pt.Open("form.pdf")
	if err != nil {
		fmt.Println(err)
		panic("Could not open pdf.")
	}
}
