from PyPDF2 import PdfFileWriter, PdfFileReader
output = PdfFileWriter()
    
ipdf = PdfFileReader(open('form.pdf', 'rb'))
manipdf = PdfFileReader(open('hello.pdf', 'rb'))
watermark = manipdf.getPage(0)
    
for i in range(ipdf.getNumPages()):
    page = ipdf.getPage(i)
    if i is 0:
        page.mergePage(watermark)
    output.addPage(page)
    
with open('newfile.pdf', 'wb') as f:
    output.write(f)

