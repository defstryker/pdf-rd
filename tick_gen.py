from reportlab.pdfgen import canvas
from reportlab.lib.units import cm, mm
c = canvas.Canvas("hello.pdf")

# row 1
c.drawString(20.928*mm, 179*mm, "✓")
c.drawString(47.273*mm, 179*mm, "✓")
c.drawString(73.125*mm, 179*mm, "✓")
c.drawString(98.977*mm, 179*mm, "✓")
c.drawString(129.261*mm, 179*mm, "✓")
c.drawString(156.098*mm, 179*mm, "✓")

# row 2
c.drawString(20.928*mm, 171.856*mm, "✓")
c.drawString(73.125*mm, 171.856*mm, "✓")
c.drawString(120.644*mm, 171.856*mm, "✓")

# row 3
c.drawString(20.928*mm, 164.715*mm, "✓")
c.drawString(47.273*mm, 164.715*mm, "✓")
c.drawString(112.026*mm, 164.715*mm, "✓")
c.drawString(142.310*mm, 164.715*mm, "✓")

# row 4
c.drawString(20.928*mm, 157.822*mm, "✓")
c.drawString(90.359*mm, 157.822*mm, "✓")
c.drawString(120.644*mm, 157.822*mm, "✓")
c.drawString(151.666*mm, 157.822*mm, "✓")

# row 5
c.drawString(20.928*mm, 150.918*mm, "✓")
c.drawString(73.125*mm, 150.918*mm, "✓")
c.drawString(129.261*mm, 150.918*mm, "✓")

# row 6
c.drawString(20.928*mm, 143.541*mm, "✓")
c.drawString(55.890*mm, 143.541*mm, "✓")
c.drawString(146.496*mm, 143.541*mm, "✓")

# row 7
c.drawString(20.928*mm, 136.155*mm, "✓")
c.drawString(73.125*mm, 136.155*mm, "✓")
c.drawString(125.075*mm, 136.155*mm, "✓")

# row 8
c.drawString(20.928*mm, 129.261*mm, "✓")
c.drawString(64.261*mm, 129.261*mm, "✓")
c.drawString(107.594*mm, 129.261*mm, "✓")
c.drawString(151.666*mm, 129.261*mm, "✓")

c.showPage()
c.save()