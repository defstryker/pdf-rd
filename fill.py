from reportlab.pdfgen import canvas
from reportlab.lib.units import cm, mm
from PyPDF2 import PdfFileWriter, PdfFileReader
from optparse import OptionParser

def question11(c, q11):
    {
        1: lambda: c.drawString(20.928*mm, 179*mm, "✓"),
        2: lambda: c.drawString(47.273*mm, 179*mm, "✓"),
        3: lambda: c.drawString(73.125*mm, 179*mm, "✓"),
        4: lambda: c.drawString(98.977*mm, 179*mm, "✓"),
        5: lambda: c.drawString(129.261*mm, 179*mm, "✓"),
        6: lambda: c.drawString(156.098*mm, 179*mm, "✓"),
        7: lambda: c.drawString(20.928*mm, 171.856*mm, "✓"),
        8: lambda: c.drawString(73.125*mm, 171.856*mm, "✓"),
        9: lambda: c.drawString(120.644*mm, 171.856*mm, "✓"),
        10: lambda: c.drawString(20.928*mm, 164.715*mm, "✓"),
        11: lambda: c.drawString(47.273*mm, 164.715*mm, "✓"),
        12: lambda: c.drawString(112.026*mm, 164.715*mm, "✓"),
        13: lambda: c.drawString(142.310*mm, 164.715*mm, "✓"),
        14: lambda: c.drawString(20.928*mm, 157.822*mm, "✓"),
        15: lambda: c.drawString(90.359*mm, 157.822*mm, "✓"),
        16: lambda: c.drawString(120.644*mm, 157.822*mm, "✓"),
        17: lambda: c.drawString(151.666*mm, 157.822*mm, "✓"),
        18: lambda: c.drawString(20.928*mm, 150.918*mm, "✓"),
        19: lambda: c.drawString(73.125*mm, 150.918*mm, "✓"),
        20: lambda: c.drawString(129.261*mm, 150.918*mm, "✓"),
        21: lambda: c.drawString(20.928*mm, 143.541*mm, "✓"),
        22: lambda: c.drawString(55.890*mm, 143.541*mm, "✓"),
        23: lambda: c.drawString(146.496*mm, 143.541*mm, "✓"),
        24: lambda: c.drawString(20.928*mm, 136.155*mm, "✓"),
        25: lambda: c.drawString(73.125*mm, 136.155*mm, "✓"),
        26: lambda: c.drawString(125.075*mm, 136.155*mm, "✓"),
        27: lambda: c.drawString(20.928*mm, 129.261*mm, "✓"),
        28: lambda: c.drawString(64.261*mm, 129.261*mm, "✓"),
        29: lambda: c.drawString(107.594*mm, 129.261*mm, "✓"),
        30: lambda: c.drawString(151.666*mm, 129.261*mm, "✓"),
    }[Q11]()

def question3(c, q3):
    # c.drawString(33.253*mm, 224.064*mm, q3)
    c.drawString(34*mm, 224.064*mm, q3)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("--q11", dest="q11",
                    help="option for question 11", metavar="QUESTION11", 
                    default=None)
    parser.add_option("--q3", dest="q3",
                    help="Name for question 3", metavar="QUESTION3", 
                    default='')

    (options, args) = parser.parse_args()

    # print(options)
    Q11 = int(options.q11)
    Q3 = options.q3
    # Q3 = "Park Sangwon"


    c = canvas.Canvas("layer.pdf")
    c.setFont("Times-Roman", 11)

    question3(c, Q3)
    question11(c, Q11)
    
    c.showPage()
    c.save()

    output = PdfFileWriter()    
    ipdf = PdfFileReader(open('form.pdf', 'rb'))
    manipdf = PdfFileReader(open('layer.pdf', 'rb'))

    page1 = manipdf.getPage(0)

    for i in range(ipdf.getNumPages()):
        page = ipdf.getPage(i)
        if i is 0 and Q11 and Q3:
            page.mergePage(page1)
        output.addPage(page)
        
    with open('output.pdf', 'wb') as f:
        output.write(f)